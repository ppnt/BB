package com.eova.cloud;

import com.eova.cloud.auth.EovaApp;
import com.eova.common.utils.JFinalUtils;

public class AuthCloud {
  public static String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCsH+a21jY5whyVZYJmsiIZwWW8efAYJH9nEHRqtJI5qsu5SkZqDOwGBu24ZZA1aRSo/yld1VgTOJK+fgStIcSYf+tnmyZfwhoxjUhQSnL+w60mYAq2sRypCPtHsZsGI4uUmWJhnpuj0CJ/VHEHAyztpHmXWu8abRpIzkK9vv9ivQIDAQAB";

  private static EovaApp app;

  public static boolean isAuthApp(String appId, String appSecret) {
    return getEovaApp().isAuth();
  }

  public static EovaApp getEovaApp() {

    if (app != null) {
      return app;
    }

    String appId = JFinalUtils.getConfig("app_id");
    String appSecret = JFinalUtils.getConfig("app_secret");
//
//			byte[] res = RSAEncrypt.decrypt(RSAEncrypt.loadPublicKeyByStr(publicKey), Base64.decode(appSecret));
//			String s = new String(res);
//			String[] ss = s.split(",");

    app = new EovaApp();
    app.setId(appId);
    app.setSecret(appSecret);
    app.setDomain(JFinalUtils.getConfig("app_domain"));
    app.setLogo(JFinalUtils.getConfig("app_logo"));
//			app.setName(ss[1]);
//			app.setCopyright(ss[2]);
//			if (appId.equalsIgnoreCase(ss[0])) {
//				app.setAuth(true);
//			}

    app.setName(JFinalUtils.getConfig("app_name"));
    app.setCopyright(JFinalUtils.getConfig("app_copyright"));

    app.setAuth(true);

    app.setKeywords(JFinalUtils.getConfig("app_keywords", JFinalUtils.getConfig("app_name")));
    app.setDescription(JFinalUtils.getConfig("app_description", JFinalUtils.getConfig("app_name")));

    return app;

  }

}