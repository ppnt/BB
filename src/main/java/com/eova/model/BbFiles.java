package com.eova.model;

import com.eova.common.base.BaseModel;

/**
* @Description:系统文件系统
* @author 作者:jzhao
* @createDate 创建时间：2021年5月15日 下午11:38:30
* @version 1.0     
*/
public class BbFiles extends BaseModel<BbFiles> {
	private static final long serialVersionUID = 1L;
	
	public static final BbFiles dao = new BbFiles();
	
	public BbFiles getByUrl(String url) {
		return this.dao().findByIdLoadColumns(url, "url");
	}
}
