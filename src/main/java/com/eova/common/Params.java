/**
 * 
 */
package com.eova.common;

import java.util.Enumeration;
import java.util.HashMap;

import com.eova.common.utils.JFinalUtils;
import com.eova.config.PageConst;
import com.jfinal.core.Controller;

/**
 * @Description: 动态入参用
 * @author Jin
 * @date 2021-4-2913:37:11
 * @version v1.0
 */
public class Params extends HashMap<String, Object> {
	
	public Params() {
	}
	public Params(Controller c) {
		this();
		Enumeration<String> e = c.getParaNames();
    	while(e.hasMoreElements()) {
    		String paramName = e.nextElement();
    		if(!JFinalUtils.isEmpty(paramName)) {
    			if(paramName.startsWith(PageConst.QUERY) || paramName.startsWith("start_") || paramName.startsWith("end_")) { //query_ 开头的查询条件
        			this.put(paramName, c.getPara(paramName));
        		}
    		}
    	}
	}
}
