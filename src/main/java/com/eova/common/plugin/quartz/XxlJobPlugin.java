package com.eova.common.plugin.quartz;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

import org.quartz.SchedulerException;

import com.eova.common.utils.JFinalUtils;
import com.jfinal.plugin.IPlugin;
import com.xxl.job.core.executor.impl.XxlJobSimpleExecutor;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class XxlJobPlugin implements IPlugin {

	private XxlJobSimpleExecutor xxlJobExecutor = null;
	
	@Override
	public boolean start() {
		
		 // load executor prop
		 try {
	        
	        // init executor
	        xxlJobExecutor = new XxlJobSimpleExecutor();
	        xxlJobExecutor.setAdminAddresses(JFinalUtils.getConfig("xxl.job.admin.addresses"));
	        xxlJobExecutor.setAccessToken(JFinalUtils.getConfig("xxl.job.accessToken"));
	        xxlJobExecutor.setAppname(JFinalUtils.getConfig("xxl.job.executor.appname"));
	        xxlJobExecutor.setAddress(JFinalUtils.getConfig("xxl.job.executor.address"));
	        xxlJobExecutor.setIp(JFinalUtils.getConfig("xxl.job.executor.ip"));
	        xxlJobExecutor.setPort(JFinalUtils.getConfigInt("xxl.job.executor.port", 9090));
	        xxlJobExecutor.setLogPath(JFinalUtils.getConfig("xxl.job.executor.logpath"));
	        xxlJobExecutor.setLogRetentionDays(JFinalUtils.getConfigInt("xxl.job.executor.logretentiondays",30));
	
	        // registry job bean
	        //xxlJobExecutor.setXxlJobBeanList(Arrays.asList(new SampleXxlJob()));
	
	        // start executor
        
            xxlJobExecutor.start();
	           
		 }catch (Exception e) {
			 log.info("启动XxlJobPlugin失败：{}",e.getCause());
			new RuntimeException(e);
		}
		return true;
	}

	@Override
	public boolean stop() {
		// TODO Auto-generated method stub
		if (xxlJobExecutor != null) {
            xxlJobExecutor.destroy();
        }
		return true;
	}
	
	
	

}
