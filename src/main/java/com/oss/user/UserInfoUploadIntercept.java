/**
 * 
 */
package com.oss.user;

import java.util.Iterator;
import java.util.List;

import com.eova.aop.AopContext;
import com.eova.common.utils.EncryptUtil;
import com.eova.common.utils.JFinalUtils;
import com.eova.common.utils.excel.ExcelUtil;
import com.eova.model.User;
import com.eova.template.single.SingleIntercept;
import com.google.common.base.Splitter;
import com.jfinal.plugin.activerecord.Record;

/**
 * @author Administrator
 *
 */
public class UserInfoUploadIntercept extends SingleIntercept {
	
	


	/**
	 * 需要处理用户信息（如果不存在的话）
	 * excel初始数据后前置任务(事务内)
	 * ac.ctrl.getAttr(ExcelUtil.IMP_EXCEL_SHEET); excel数据如果需要再次处理的话
	 * ac.ctrl.getAttr(ExcelUtil.IMP_EXCEL_SHEET_HEAD) 头信息
	 * Record.get(ExcelUtil.SHEET_ROWNO) 数据对应的 excel行号 注意：：：：：：如果有拦截器处理完业务需要删除掉ExcelUtil.SHEET_ROWNO属性，否则保存会报错
	 * ExcelUtil.getSheetRowNameValue 获取需要的列数据
	 * @param ac 
	 * @throws Exception
	 */
	public void importInit(AopContext ac) throws Exception {
		super.importInit(ac);
		
		List<Record> records = ac.records;
		
		for(Iterator<Record> it =records.iterator();it.hasNext();) {
			Record record = it.next();
			
			String rids = record.getStr("rids");
			if(!JFinalUtils.isEmpty(rids)) {
				record.set("rid", rids.split(",")[0]);
			}
		}
		
		
	}
	
	/**
	 * 导入后置任务(事务内)
	 * 
	 */
	public void importAfter(AopContext ac) throws Exception {
		//分2种情况，新增和 修改
		String psw = JFinalUtils.getConfig("user_df_psw", "000000");
		for (Record record : ac.records) {
			User lUser = User.dao.findById(record.get("id"));
			String rids = record.get("rids");
			if(lUser == null) {
				
				
				lUser = new User();
				lUser.set("login_id", record.get("login_id"));
				lUser.set("login_pwd", EncryptUtil.getSM32(psw));
				lUser.set("rids", rids);
				if(!JFinalUtils.isEmpty(rids)) {
					lUser.set("rid", Splitter.on(",").splitToList(rids).get(0));
				}
				lUser.set("id", record.get("id"));
				
				lUser.save();
			}else {
					lUser.set("rids", rids);
					lUser.set("login_id", record.get("login_id"));
					
					if(!JFinalUtils.isEmpty(rids)) {
						lUser.set("rid", Splitter.on(",").splitToList(rids).get(0));
					}
					
					lUser.update();
			}
		}
		
	}
	
}
