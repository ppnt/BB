package com;

import com.jfinal.server.undertow.UndertowServer;
import com.oss.OSSConfig;

/**
 * 鼠标右键->Run As Java Application
 *
 */
public class RunEovaOSS {
  public static void main(String[] args) {
    long start = System.currentTimeMillis();
    
    // 启动Jetty服务器
    // JFinal.start("src/main/webapp", 801, "/", 0);
    
    //启动Undertow服务器
    UndertowServer.create(OSSConfig.class).addHotSwapClassPrefix("org.beetl.").start();
    long end = System.currentTimeMillis();
    System.out.println("共使用了" + (end - start) + "ms");
  }
}
